# group_02_project

## Project Work Summary

### **Nick Messina** (messina4@illinois.edu)

### Part 1

- Helped IlliniBlockChain members with project workflow pieces:
  - Getting set up with VM
  - Using the VPN
  - Setting up Yubikeys
  - Setting up SSH
  - Getting access to the GitLab
- Created [guides](https://drive.google.com/drive/folders/1IaGvKstp-s9vCYXpomcgMC9QO_vivU1U) and videos detailing how to get connected and the main parts of the file system
- Helped other members in the process of making a strategy
- Helped troubleshoot problems others ran into and answererd questions

### Part 2

- Fixed issues with crypto data not being able to be parsed and used in the backtests
- Modified the hyperparameter tuning project workflow from [group_03](https://gitlab.engr.illinois.edu/ie497_ie597_independent_study_spring_2024/ie497_ie597_spring_2024_group_03/group_03_project/-/tree/main?ref_type=heads) to be used with crypto data and our groups strategies.
  - First, I got the tuning and crypto data working on a simple provided strategy
  - Then, I replicated the process on the strategy created by the group (7_SMAStrategy)

### **Savya Raghavendra** (savyar2@illinois.edu)

### Part 1

- Helped Illini BlockChain members with project workflow pieces:
  - Getting set up with VM
  - Using the VPN
  - Setting up Yubikeys
  - Setting up SSH
  - Getting access to the GitLab
- Created [guides](https://drive.google.com/drive/folders/1IaGvKstp-s9vCYXpomcgMC9QO_vivU1U) and videos detailing how to get connected and the main parts of the file system

### Part 2

- Helped manage workflows
  - Manage creation of strategy
  - Gain access to level 2 order book data
  - Manage hypertuning
- Secured various external partnerships (sprk.fi, Metis DEX, Monad DEX) to deploy our algorithms on

### **Austin Abraham** (austina5@illinois.edu)

- Research: Conducted in-depth research into various strategic approaches.
- Planning: Developed a comprehensive implementation plan for the project.
- Initiation: Began the creation of essential project files.
- Strategy Development Assistance: Provided support in refining and coding strategic components.

### **Saketh Boyapally** (sakethb2@illinois.edu)

- Coding: Took primary responsibility for coding the core strategy of the project.

### **Shray Srivivasta** (ssriv5@illinois.edu)

- Research: Conducted in-depth research into various strategic approaches.
- Planning: Developed a comprehensive implementation plan for the project.
- Coding: Provided key support for coding the core strategy of the project

### **Siddhant Wanjara** (swanj2@illinois.edu)

- Research Support: Assisted in the strategic research, contributing to foundational insights. 
- Strategy Implementation: Played a key role in the practical application and refinement of the strategy.
- Hypertune Research: Assisted in laying out the framework for hypertuning scripts

### **Matt Partridge** (mp52@illinois.edu)

- Research Support: Assisted in strategy research, and laying out framework
- Hypertune Research: Assisted in laying out the framework for hypertuning scripts
- Coding: Assisted in key areas for strategy and hypertuning scripts

