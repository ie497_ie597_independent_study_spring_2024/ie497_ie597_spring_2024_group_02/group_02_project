## Introduction

In the new and rapidly evolving world of cryptocurrency trading, knowing how to navigate this volatile market as opposed to traditional markets is crucial. This project focuses on the creation and backtesting of a market making algorithm made for the cryptocurrency market. Unlike traditional stock markets, the crypto market is characterized by unique differences and opportunities: its volatility, 24/7 market hours, and a landscape influenced differently by retail and institutional investors. These distinct features are interesting to take on with a market making strategy.

The primary goal of this algorithm is to generate profit. This objective, while obvious, is complicated by the unpredictability and liquidity variations in the crypto market. We are testing various models and algorithms, to find out which holds best in the market's unique characteristics.

Our approach also includes extensive backtesting across a spectrum of market conditions, going beyond the typical bull and bear market scenarios. For example, we backtested with 2020 data, during COVID, which contained most irregular price movements within a market, amplified in cryptocurrency. This testing is essential to test for the algorithm's robustness and reliability across different market phases. Another significant part of this project involves the application of data science skills: acquiring, extracting, and formatting trading data from cryptocurrency exchanges like Gate.io for use with Strategy Studio software. This process not only tests our development skills but also in handling and analyzing real-world financial data.

This paper will cover several key areas:

- **Background**: We will start by explaining what market making is, how it generates profit, and foundational elements of what is involved.
- **Different Models**: Various market making algorithms will be analyzed, and tested within the market.
- **Data Collection**: The methodology for sourcing and preparing data from cryptocurrency exchanges will be detailed.
- **Backtesting**: We will outline our approach to backtesting the developed algorithms.
- **Final Results/Analysis**: The outcomes of our backtesting will be presented and analyzed, evaluating the performance of the algorithms.
- **Conclusion**: The paper will conclude with a summary of our findings, the contributions of our project to the field of cryptocurrency trading, and potential avenues for future research.

---

### Background

#### General Concept of Market Making

Market making is an essential function in financial markets, by continously providing buy and sell orders to facilitate trading. Market makers add liquidity to the market, which is essential for ensuring active trading and price stability. By offering to buy and sell securities/assets, they bridge the gap between buyers and sellers.

#### Liquidity

Liquidity refers to the ease with which assets can be bought or sold in a market without causing a significant movement in the price. In the context of market making, liquidity is important for minimizing trading costs and keeping efficient market operations. Market makers play a key role in maintaining this liquidity. By always being ready to buy or sell, they ensure market participants can execute trades while keeping the price stable.

#### Order Books

An order book is a list of buy and sell orders for a specific asset, organized by price level. Market makers contribute to these books by placing orders.
<img src="Order.png" alt="Semantic description of image" width="300"/>

For example, in this order book, if someone places a buy order at $100.02 for 800 shares, it will execute at $100 and 1000 shares will become 200. Another example is if someone places a sell order at market price for 160 shares, 100 shares will execute at $99.95, 50 will execute at $99.90, and 10 will execute at $99.85.

https://www.youtube.com/watch?app=desktop&v=Kl4-VJ2K8Ik

#### Bid/Ask Prices

The bid price is the highest price a market maker is willing to pay to buy an asset, while the ask price is the lowest price at which they are willing to sell. The difference between these prices, known as the spread, is crucial for a market maker's profitability.

#### Spread

The spread, the difference between the bid and ask prices, is a key element in market making. Market makers earn their profits primarily through capturing this spread. By buying at the bid price and selling at the ask price, they benefit from the price difference between buying and selling orders.

#### Profit Generation and Strategy

Market makers aim to generate profit primarily through the spread. Managing this profitably involves complicated inventory management strategies to mitigate risk. By balancing their buy and sell orders, market makers ensure they are not overly exposed to market movements, which could lead to significant losses. If a market maker has a certain amount of inventory, they might hedge it by buying puts (predicting the market will go down), which ensures they don't lose money if the price goes down dramatically. If the price goes up dramatically, their inventory is the hedge (since it is now worth more) to ensure no loss.

#### Alternative Objectives in Market Making

Not all market making is solely profit-driven. For instance, some hedge funds engage in market making with the objective of increasing trading volume on a particular exchange. This increased volume may lead to better fee arrangements with the exchange, benefitting other trading desks within the firm by reducing overall trading costs.

#### Types of Market Making Strategies

There are various market making strategies, including quote-driven, high-frequency trading (HFT), electronic, and order-driven. This project will mainly focus on HFT strategies, particularly in the context of the cryptocurrency market.

#### Price Discovery

Market makers also aid in the process of price discovery, where the market price of an asset is determined through the interactions of buy and sell orders. In volatile or illiquid markets, market makers play a crucial role in ensuring that prices reflect the current supply and demand dynamics.

### Different Market Making Models

#### Avellanada-Stoikov Model

The Avellaneda-Stoikov market-making model is a significant contribution to the field of quantitative finance, particularly in the area of algorithmic trading. Introduced by Marco Avellaneda and Sasha Stoikov in 2008, this model provides a framework for setting optimal bid and ask quotes in a limit order book market. Here’s more detail: https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_07/group_07_project/-/blob/market_making/AS%20Model%20Research.pdf

we use parameter:

- $s$ the per-unit mid-price of the
- $T$, the terminus of the time series
- $\sigma$, the volatility of the asset
- $q$, the number of assets held in inventory
- $\gamma$, a risk factor that is adjusted to meet the risk/return trade-off of the market maker
- $x$, the initial capital of the market maker
- $k$, the intensity of the arrival of orders

for every seconds, we calculate the reservation price on both sides, when the market price is higher than our ask price, we sell 1 unit, when a market order is lower than our bid price, we buy 1 unit, finally we want to control the inventory batween a certain threshold.

#### Sigmoid Model

A sigmoid function is a mathematical function having a characteristic "S"-shaped curve or sigmoid curve. It's mathematical representation is:

$f(x) = \frac{1}{1 + e^{-x}}$

You can use this function as a reaction to the bid/ask price, and adjust the size of the position you buy/sell.

##### Results

Parameters

- Instrument: SPY
- Date: 2019-01-30
- Position Cap: 5000
- PnL Sharpe Ratio: 0.06548127150741091
  ![2019-01-30-10000-500](./4_SigmoidStrategy/pnl_analysis/PNL_Screenshot.png)


#### Basic Order Flow Imbalance Model

##### Model Summary

- **Definition**: Order flow imbalance refers to a situation where there is a significant difference between the volume of buy orders and sell orders for a financial instrument. This concept is crucial in algorithmic trading and market microstructure analysis because it provides insights into the supply and demand dynamics in the market and can be indicative of future price movements.
- **How it measured**: It is typically measured over a specific time frame, and can be quantified in various ways, such as the difference in the number of buy and sell orders, the total volume of buy vs. sell orders, or the aggregate value of buy and sell orders.

##### Code Explaination

The `OnDepth` event is called whenever there is a change in the orderbook. When this event if called:

- The strategy gets the `best_bid_price` and the `best_ask_price` (NBBO) of the orderbook. It also get the quantity of the orders at those price levels i.e. `best_bid_size` & `best_ask_size`.
- It then calculates `imbalance` as `best_bid_size - best_ask_size`
- If the absolute value of `imbalance` is greater than a user defined `threshold` then:

  - If the `imbalance > 0` i.e. there is an imbalance of more buy orders than sell orders:
    - We create sell orders at the `best_ask_price` to rebalance the orderbook
  - If the `imbalance < 0` i.e. there is an imbalance of more sell orders than buy orders:

    - We create buy orders at the `best_bid_price` to rebalance the orderbook

  - Note: There is a user defined `max_buy_position` and `max_sell_position` to limit the number of orders the strategy creates.

##### Code Reasoning

The strategys reasoning is centered on the role of a market maker in providing liquidity to the market. By responding to changes in the orderbook (via the OnDepth event), the strategy actively manages order imbalances. This is achieved by comparing best_bid_size and best_ask_size to detect imbalances. When an imbalance exceeds a certain threshold, the strategy intervenes by creating orders—selling when there's an excess of buy orders, and buying when there are more sell orders.

The strategy is based on the thought that if there are more buy orders than sell orders, there must be people wanting to buy. Therefore, we create more sell orders since people are looking to buy. Same goes for the opposite. If there are more sell orders than buy orders, people are wanting to sell. So, we create more buy orders since people are looking to sell.

##### Results

#### PnL Plots

Parameters

- Instrument: SPY
- Date: 2019-01-30
- Position Cap: 10000
- Threshold: 500
- PnL Sharpe Ratio: -0.30607

![2019-01-30-10000-500](./0_OfiStrategy/strategy_analysis/plots/pnl-2019-01-30-2019-01-30-10000-500.png)

Parameters

- Instrument: SPY
- Date: 2022-05-02 - 2022-05-06
- Position Cap: 10000
- Threshold: 1000
- PnL Sharpe Ratio: 1.28250

![2022-05-02-2022-05-06-10000-1000](./0_OfiStrategy/strategy_analysis/plots/pnl-2022-05-02-2022-05-06-10000-1000.png)

##### Possible Next Steps for Model

- Add more parameters
  - X number of levels included in imbalance consideration
  - Threshold could become a ratio instead of a fixed value
  - Stop loss becoming a function instead of just a capped position
- Order cancellation when market imbalance naturally corrects
- Hyperparameter tuning for all parameters
- Imbalance in other correlated instruments can be detected to also be factored

#### Avellaneda Stoikov Model

##### Model Summary

for every second we calculate the reservation price based on the midprice and 5 other parameters, after that when the lowest price of that second lower than the bid price we buy 1 unit and when the highest price of that second higher than the ask price we sell 1 unit and keep the inventory within the MAX_POSITION.

##### Results

Parameters

- Instrument: SPY
- Date: 2019-01-30
- Position Cap: 10000
- PnL Sharpe Ratio: 0.9403262708342492
  ![2019-01-30-10000-500](./2_ASStrategy/pnl_analysis/plot/pnl.png)

#### SMA Strategy

##### Model Summary

A strategy modeled off of the SimoidStrategy for backtesting crypto markets could involve using simple moving average (SMA) crossovers to generate buy and sell signals. Here's a simplified version of how you could implement such a strategy:

1. Strategy Definition:
Use two SMAs of different periods, such as a shorter-term SMA (e.g., 20-period) and a longer-term SMA (e.g., 50-period).
Generate buy signals when the shorter-term SMA crosses above the longer-term SMA (a bullish crossover).
Generate sell signals when the shorter-term SMA crosses below the longer-term SMA (a bearish crossover).
2. Data Collection:
Gather historical price data for the cryptocurrency you want to backtest the strategy on. You'll need price data for the time period you want to analyze.
3. Signal Generation:
Calculate the SMAs for both the shorter and longer periods based on the historical price data.
Generate buy signals when the shorter-term SMA crosses above the longer-term SMA and sell signals when the opposite occurs.
4. Backtesting:
Simulate trading based on the generated buy and sell signals over the historical data.
Keep track of hypothetical trades, entry and exit points, and profits or losses.
5. Performance Analysis:
Analyze the performance of the strategy based on backtest results.
Calculate metrics such as total profit or loss, win rate, maximum drawdown, and risk-adjusted returns.
6. Optimization:
Optionally, optimize the strategy by adjusting parameters such as the lengths of the SMAs to improve performance based on backtest results.
7. Out-of-Sample Testing:
Validate the performance of the optimized strategy on a separate set of historical data that wasn't used in the initial backtest.
8. Paper Trading or Live Testing:
Once satisfied with the performance in out-of-sample testing, consider paper trading or conducting live testing with a small amount of capital to further validate the strategy's effectiveness in real-world conditions.

### Crypto Data

We used Binance level 2 order book data from Group 5's FIN 556 project last semester.

Directly quoted from their final report (https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_05/group_05_project/-/blob/main/Final_Report/final_report.md):
"Thanks to one of our team members, Zhicheng, we acquired Binance crypto data from the company where he is interning. This made our task a lot easier. We chose Bitcoin's data because it is the best-known and most liquid of the cryptocurrencies. In our data, we decided to use incremental_book_L2, which has Level 2 order book data for every order book price level update. The Level 2 order book allows us to construct the total Bitcoin order book at every price level and provides us with every price level book update, allowing us to implement a more well-thought-out strategy and make the backtest more accurate. Our Level 2 order book data is given in single-day batches, so the first few thousand entries in our original data have the is_snapshot flag set to TRUE (is_snapshot = TRUE if that update is filling the order book with the previous day's order book state). The rest of the columns in the data are self explanatory. A screenshot of the L2 book:"
![Alt text](image.png)

### Hypertuning Scripts

We wrote hypertuning scripts which essentially backtests the different strategies, changing the various parameters and calculating the effect of the change on PNL, and ultimately calculates the most ideal combination of parameters for highest profitability.

### Further Steps/Current Ventures

We are currently working with sprk.fi (a perpetuals DEX on Fuel Network) to improve the parameter tuning process, and deploy our Market Making algorithms on their DEX. We are also going to deploy our algorithms on a DEX on the Metis blockchain, and another DEX on Monad. We are also in the process of launching our own perpetual DEX on Monad.
